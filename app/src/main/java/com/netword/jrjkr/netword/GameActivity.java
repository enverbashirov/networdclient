package com.netword.jrjkr.netword;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by Taithin on 14.12.2016.
 */

public class GameActivity extends AppCompatActivity implements View.OnClickListener{

    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;

    private TextView playerInputField;
    private TextView enemyInputField;
    private TextView scorePlayer1;
    private TextView scorePlayer2;
    private TextView bonusLetter;
    private TextView player1TimeLeft;

    private Button sendWord;

    public int playerTurn = 1;
    public int player1Score = 0;
    public int player2Score = 0;
    public String bnsLetter;
    public String playerWord;
    public String enemyWord;

    FirebaseUser user = firebaseAuth.getCurrentUser();


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        playerInputField = (TextView) findViewById(R.id.playerInputField);
        enemyInputField = (TextView) findViewById(R.id.enemyInputField);
        scorePlayer1 = (TextView) findViewById(R.id.scorePlayer1);
        scorePlayer2 = (TextView) findViewById(R.id.scorePlayer2);
        bonusLetter = (TextView) findViewById(R.id.bonusLetter);
        player1TimeLeft = (TextView) findViewById(R.id.player1TimeLeft);

        sendWord = (Button) findViewById(R.id.sendWord);
        sendWord.setOnClickListener(this);


        final Intent main = new Intent(this, MainActivity.class);

        bonusLetter.setText(bonusLetterGeneration());
        new CountDownTimer(60000, 1000){
            public void onTick(long millisUntilFinished){
                String remainingTime = Long.toString(millisUntilFinished / 1000);
                player1TimeLeft.setText(remainingTime);
            }

            public void onFinish(){
                player1TimeLeft.setText("END");
                startActivity(main);
            }
        }.start();

        mDatabase.child("matchmaking").child("match1").child("wordCount").setValue(1);

        turnCalculateAtStart();
        setEditingEnabled();

    }

    //at start send turn numbers to respective users
    private void turnCalculateAtStart(){
        // TODO: Change child("match1") later
        mDatabase.child("matchmaking").child("match1").child("player1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == user.getUid()) {
                    playerTurn = 1;
                    mDatabase.child("matchmaking").child("match1").child("player1turn").setValue(1);
                }
                else{
                    playerTurn = 2;
                    mDatabase.child("matchmaking").child("match1").child("player2turn").setValue(2);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    //hopefully swaps the player turns
    private void turnSwap(){
        mDatabase.child("matchmaking").child("match1").child("player1turn").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == 1){
                    mDatabase.child("matchmaking").child("match1").child("player" + dataSnapshot.getValue() + "turn").setValue(2);
                    setTurn(2);
                }
                else{
                    mDatabase.child("matchmaking").child("match2").child("player" + dataSnapshot.getValue() + "turn").setValue(1);
                    setTurn(1);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    //player 1 and player 2 should be able to use this by turn
    private void submitWord() {
        if(getTurn() == 1){
            setEditingEnabled();
            playerWord = playerInputField.getText().toString().trim();

            if (playerWord.length() <= 3) {
                Toast.makeText(this, "Word should be more than 3 letters", Toast.LENGTH_SHORT).show();
            }

            //pushes entered words in to database
            mDatabase.child("matchmaking").child("match1").child("wordCount").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mDatabase.child("matchmaking").child("match1").child("word" + dataSnapshot.getValue()).setValue(playerWord);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //calculates point of the word
            mDatabase.child("matchmaking").child("match1").child("player1turn").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getValue() == 1) {
                        player1Score += pointCalculation(playerWord);
                        scorePlayer1.setText(player1Score);
                    }
                    else {
                        player2Score += pointCalculation(playerWord);
                        scorePlayer2.setText(player2Score);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });

        }
        while(getTurn() == 2){

        }

    }

    //calculates point of the input
    public int pointCalculation(String word){
        int totalPoint = 0;
        int wordLength = 0;
        int tmpInt = 0;

        wordLength = word.length();
        if(wordLength == 3){
            totalPoint = 30;
        }else if (wordLength == 4){
            totalPoint = 50;
        }else if (wordLength == 5){
            totalPoint = 70;
        }else if (wordLength == 6){
            totalPoint = 90;
        }else if (wordLength == 7){
            totalPoint = 140;
        }else if (wordLength == 8){
            totalPoint = 190;
        }else if (wordLength == 9) {
            totalPoint = 240;
        }else if (wordLength >= 10){
            tmpInt = wordLength - 10;
            totalPoint = 290 + (tmpInt * 85);
        }

        if(word.contains(bnsLetter)){
            totalPoint = totalPoint*2;
        }
        return totalPoint;
    }

    //creates a bonus letter for the game. If input contains this letter point of that word doubles.
    public String bonusLetterGeneration(){
        Random rng = new Random();
        StringBuilder bonusLetter = new StringBuilder();
        char tempChar;
        tempChar = (char) (rng.nextInt(96) + 32);
        bonusLetter.append(tempChar);
        return bonusLetter.toString();
    }

    private void setEditingEnabled() {
        mDatabase.child("matchmaking").child("match1").child("player1").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == user.getUid() && getTurn() == 1)
                    playerInputField.setFocusable(true);
                else if(dataSnapshot.getValue() == user.getUid() && getTurn() == 2)
                    playerInputField.setFocusable(false);
                else if(dataSnapshot.getValue() != user.getUid() && getTurn() == 1)
                    playerInputField.setFocusable(false);
                else if(dataSnapshot.getValue() != user.getUid() && getTurn() == 2)
                    playerInputField.setFocusable(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    private int getTurn(){
        return playerTurn;
    }

    private void setTurn(int turn){
        playerTurn = turn;
    }

    @Override
    public void onClick(View v) {
        if (v == sendWord) {
            //submits word
            submitWord();
            //generates bonus letter
            bonusLetter.setText(bonusLetterGeneration());
            //swap turns
            turnSwap();
            //set focusability of the text field according to turn
            setEditingEnabled();
        }
    }
}
