package com.netword.jrjkr.netword;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private User mUser;

    private FirebaseAuth firebaseAuth;

    private Bitmap chosenImage = null;
    private Uri pictureUri;

    private ImageView userProfilePic;

    private TextView fName;
    private TextView sName;
    private TextView uName;

    private Button editButton;
    private ImageButton logoutButton;
    private ImageButton backButton;
    private ImageButton galleryButton;
    private ImageButton cameraButton;

    public static final int IMAGE_GALLERY_REQUEST = 20;
    public static final int CAMERA_REQUEST = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        mUser = new User();
        mUser.downloadInfo(firebaseAuth.getCurrentUser().getUid());

        fName = (TextView) findViewById(R.id.fName);
        sName = (TextView) findViewById(R.id.sName);
        uName = (TextView) findViewById(R.id.uName);

        editButton = (Button) findViewById(R.id.editButton);
        logoutButton = (ImageButton) findViewById(R.id.logoutButton);
        backButton = (ImageButton) findViewById(R.id.backButton);
        galleryButton = (ImageButton) findViewById(R.id.galleryButton);
        cameraButton = (ImageButton) findViewById(R.id.cameraButton);
        userProfilePic = (ImageView) findViewById(R.id.user_profile_photo);

        editButton.setOnClickListener(this);
        logoutButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        galleryButton.setOnClickListener( this);
        cameraButton.setOnClickListener( this);
    }

    @Override
    public void onClick(View v) {

        if (v == logoutButton) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        else if (v == backButton) {
            finish();
            startActivity( new Intent(this, MenuActivity.class));
        }
        else if (v == editButton) {
            submitPost();
        }
        else if (v == galleryButton) {
            onGalleryButtonClicked();
        }
        else if (v == cameraButton) {
            onCameraButtonClicked();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_GALLERY_REQUEST) {

                pictureUri = data.getData();

                InputStream inputStream;

                try {
                    inputStream = getContentResolver().openInputStream(pictureUri);

                    userProfilePic.setScaleType(ImageView.ScaleType.CENTER);
                    chosenImage = BitmapFactory.decodeStream(inputStream);
                    userProfilePic.setImageBitmap(mUser.getCircularBitmap(Bitmap.createScaledBitmap(chosenImage, 230, 230, true)));

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    // show a message to the user indictating that the image is unavailable.
                    Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                }
            }
            if (requestCode == CAMERA_REQUEST) {
                if (pictureUri != null) {
                    try {
                        chosenImage = BitmapFactory.decodeStream(getContentResolver().openInputStream(pictureUri));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        // show a message to the user indictating that the image is unavailable.
                        Toast.makeText(this, "Unable to open image", Toast.LENGTH_LONG).show();
                    }

                    userProfilePic.setImageBitmap(mUser.getCircularBitmap(chosenImage));
                }
            }
        }
    }

    private void submitPost() {
        final String firstName = fName.getText().toString().trim();
        final String surName = sName.getText().toString().trim();
        final String userName = uName.getText().toString().trim();

        setEditingEnabled(false);

        //Uploading text information
        if ( firstName.length() >= 15)
            Toast.makeText(this, "Firstname should be less than 16 letters long", Toast.LENGTH_SHORT).show();
        else if ( surName.length() >= 15)
            Toast.makeText(this, "Surname should be less than 16 letters long", Toast.LENGTH_SHORT).show();
        else if ( userName.length() >= 15)
            Toast.makeText(this, "Username should be less than 16 letters long", Toast.LENGTH_SHORT).show();
        else {
            Toast.makeText(this, "Updating details...", Toast.LENGTH_SHORT).show();

            mUser = new User(firstName, surName, userName, chosenImage);
            mUser.uploadInfo(firebaseAuth.getCurrentUser().getUid());

            Toast.makeText(this, "Details Updated", Toast.LENGTH_SHORT).show();
        }
        setEditingEnabled(true);
    }

    private void setEditingEnabled(boolean enabled) {
        uName.setEnabled(enabled);
        sName.setEnabled(enabled);
        fName.setEnabled(enabled);
        if (enabled) {
            editButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
        }
    }

    public void onGalleryButtonClicked() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureDirectoryPath = pictureDirectory.getPath();

        Uri data = Uri.parse(pictureDirectoryPath);

        photoPickerIntent.setDataAndType(data, "image/*");

        startActivityForResult(photoPickerIntent, IMAGE_GALLERY_REQUEST);
    }

    public void onCameraButtonClicked() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File imageFile = new File(Environment.getExternalStorageDirectory(), getPictureName());
        pictureUri = Uri.fromFile(imageFile);

        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);

        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private String getPictureName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH);
        String timestamp = sdf.format(new java.util.Date());
        return "NetWord_" + timestamp + ".jpg";
    }
}