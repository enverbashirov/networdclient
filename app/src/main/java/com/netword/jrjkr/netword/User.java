package com.netword.jrjkr.netword;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JrJKR on 08/12/2016.
 *
 * Last Edited on 10/12/2016
 * Cheers
 */

class User implements UserImplementer{

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    //User Personal Information
    private String firstname;
    private String surname;
    private String username;
    private String compressedAvatar; //compressed bitmap string
    private String id;

    private Bitmap bitmapAvatar;

    //CONSTRUCTORS
    User() {
        firstname = surname = username = compressedAvatar = id = null;
        bitmapAvatar = null;
    }
    private User(String firstname, String surname, String username, String compressedAvatar){
        this.firstname = firstname;
        this.surname = surname;
        this.username = username;
        this.compressedAvatar = compressedAvatar;
        this.id = null;
        this.bitmapAvatar = null;
    }
    User(String firstname, String surname, String username, Bitmap bitmapAvatar){
        this.firstname = firstname;
        this.surname = surname;
        this.username = username;
        setAvatar(bitmapAvatar);
        this.id = null;
    }
    User(String firstname, String surname, String username, Bitmap bitmapAvatar, String id){
        this.firstname = firstname;
        this.surname = surname;
        this.username = username;
        setAvatar(bitmapAvatar);
        this.id = id;
    }

    //GETTERS
    public String getFirstname() {
        return this.firstname;
    }
    public String getSurname() {
        return this.surname;
    }
    public String getUsername() {
        return this.username;
    }
    public String getCompressedAvatar() {
        return this.compressedAvatar;
    }
    public Bitmap getBitmapAvatar() {
        return this.bitmapAvatar;
    }
    public String getId() {
        return this.id;
    }

    //SETTERS
    public void setFirstname( String firstname) {
        this.firstname = firstname;
    }
    public void setSurname( String surname) {
        this.surname = surname;
    }
    public void setUsername( String username) {
        this.username = username;
    }
    public void setAvatar(Bitmap bitmapAvatar) {
        this.bitmapAvatar = bitmapAvatar;

        if ( bitmapAvatar != null) {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inSampleSize = 8; // shrink it down otherwise we will use stupid amounts of memory
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmapAvatar.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] bytes = baos.toByteArray();
            compressedAvatar = Base64.encodeToString(bytes, Base64.DEFAULT);
        }
    }
    public void setAvatar(String compressedAvatar) {
        this.compressedAvatar = compressedAvatar;
        byte[] imageAsBytes = Base64.decode(compressedAvatar.getBytes(), Base64.DEFAULT);
        this.bitmapAvatar = getCircularBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
    }
    public void setId( String id) {
        this.id = id;
    }

    public void setUser( User user) {
        this.firstname = user.getFirstname();
        this.surname = user.getSurname();
        this.username = user.getUsername();
        this.id = user.getId();
        setAvatar(user.getBitmapAvatar());
    }
    public void setUser( String firstname, String surname, String username, Bitmap bitmapAvatar, String id) {
        this.firstname = firstname;
        this.surname = surname;
        this.username = username;
        this.id = id;
        setAvatar(bitmapAvatar);
    }

    //METHODS
    public void downloadInfo( String uid) {
        mDatabase.child("users").child(uid).child("firstname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null)
                    setFirstname((String) snapshot.getValue());
//                    System.out.println(getFirstname());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        mDatabase.child("users").child(uid).child("surname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null)
                    setSurname((String) snapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        mDatabase.child("users").child(uid).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null)
                    username = (String) snapshot.getValue();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        mDatabase.child("users").child(uid).child("compressedAvatar").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null)
                    setAvatar((String) snapshot.getValue());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
        this.id = uid;
    }
    public void uploadInfo(){
        if ( id != null) {
            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

            mDatabase.child("users").child(this.id).setValue(new User(this.firstname, this.surname, this.username, this.compressedAvatar));
        } else
            System.out.println("Cannot upload, id is null!");
    }
    public void uploadInfo(String uid){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("users").child(uid).setValue( new User(this.firstname, this.surname, this.username, this.compressedAvatar));
    }

//    public void uploadToCatalog(final User user){
//        mDatabase.child("users").child("userlist").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                if (snapshot != null) {
//                    if ( user.getUsername() != null) {
//                        ArrayList<User> userList = new ArrayList<>();
//                        userList.add(user);
//                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
//                        mDatabase.child("users").child("userlist").setValue(userList);
//                    }
//                } else {
//                    if ( user.getUsername() != null) {
//                        @SuppressWarnings("unchecked")
//                        ArrayList<User> userList = (ArrayList<User>) snapshot.getValue();
//                        userList.add(user);
//                        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
//                        mDatabase.child("users").child("userlist").setValue(userList);
//                    }
//                }
//            }
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                System.out.println("The read failed: " + databaseError.getCode());
//            }
//        });
//    }

    public Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }
    public void getAvatarView(final ImageView view, final Integer x, final Integer y) {
        mDatabase.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("compressedAvatar").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                String base64Image = (String) snapshot.getValue();
                if ( base64Image != null) {
                    byte[] imageAsBytes = Base64.decode(base64Image.getBytes(), Base64.DEFAULT);
                    Bitmap chosenImage = getCircularBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
                    chosenImage = Bitmap.createScaledBitmap(chosenImage, x, y, true);
                    view.setScaleType(ImageView.ScaleType.CENTER);
                    view.setImageBitmap(chosenImage);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
    public void getWelcomeIntro(final TextView v, String uid, final ImageView iv, final Integer x, final Integer y) {
        mDatabase.child("users").child(uid).child("firstname").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (snapshot.getValue() != null) {
                    v.setText("Welcome, " + (String) snapshot.getValue());
                    getAvatarView(iv, x, y);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }
}
