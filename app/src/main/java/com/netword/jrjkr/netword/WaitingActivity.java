package com.netword.jrjkr.netword;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by Taithin on 06.12.2016.
 */
public class WaitingActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "WaitActivity\t||";
    private long matchID = -1;
    private String matchName;
    private Button btnCancel;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference query;

    ValueEventListener lOnCreate;
    ValueEventListener lgetMatch;
    ValueEventListener lcheckMatch;
    ValueEventListener lcheckLobby;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait);

        firebaseAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        query = mDatabase.child("matchmaking");

        final FirebaseUser user = firebaseAuth.getCurrentUser();

        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener( this);

        getMatch();

        query.child("queue").child("player1").addValueEventListener(lOnCreate = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                if (matchID != -1) {
                    if (!snapshot.exists()) {
                        Log.i(TAG, "Player1: onCreate " + firebaseAuth.getCurrentUser().getUid());
                        query.child("queue").child("player1").setValue(user.getUid());
                        query.child("queue").child("player1").removeEventListener(this);
                        checkLobby();
                    } else {
                        Log.i(TAG, "Player2: onCreate " + firebaseAuth.getCurrentUser().getUid());
                        query.child(matchName).child("player1").setValue((String) snapshot.getValue());
                        query.child(matchName).child("player2").setValue(user.getUid());
                        query.child(matchName).child("go").setValue("wait");
                        query.child("queue").child("player1").removeEventListener(this);
                        checkMatch();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
        Log.i(TAG, "onCreate END" + firebaseAuth.getCurrentUser().getUid());
    }

    //Player 1
    public void checkLobby() {
        Log.i(TAG, "Player1: checkLobby" + firebaseAuth.getCurrentUser().getUid());
        query.child(matchName).child("go").addValueEventListener( lcheckLobby = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.i(TAG, "Player1: checkLobby IDLE " + firebaseAuth.getCurrentUser().getUid());
                if (snapshot.exists()) {
                    if ( snapshot.getValue().equals("wait")) {
                        Log.i(TAG, "Player1: checkLobby " + firebaseAuth.getCurrentUser().getUid());
                        query.child(matchName).child("go").setValue("start");
                        query.child("queue").child("player1").removeValue();
                        finish();
                        startActivity(new Intent(WaitingActivity.this, GameActivity.class).putExtra("matchName", matchName));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    //Player 2
    public void checkMatch() {
        Log.i(TAG, "Player2: checkMatch Remove Listener OnCreate " + firebaseAuth.getCurrentUser().getUid());
        query.child(matchName).child("go").addValueEventListener( lcheckMatch = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.i(TAG, "Player2: checkMatch IDLE " + firebaseAuth.getCurrentUser().getUid());
                if (snapshot.exists()) {
                    if ( snapshot.getValue().equals("start")) {
                        Log.i(TAG, "Player2: checkMatch " + firebaseAuth.getCurrentUser().getUid());
                        setMatch();
                        query.child(matchName).child("go").removeEventListener(this);
                        finish();
                        startActivity(new Intent(WaitingActivity.this, GameActivity.class).putExtra("matchName", matchName));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void getMatch() {
        query.child("matchId").addListenerForSingleValueEvent( lgetMatch = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.i(TAG, "getMatchId: " + firebaseAuth.getCurrentUser().getUid());
                if (snapshot.exists()) {
                    Log.i(TAG, "getMatchId if: " + snapshot.getValue() + firebaseAuth.getCurrentUser().getUid());
                    matchID = (Long) snapshot.getValue();
                    Log.i(TAG, "getMatchId if: " + matchID + firebaseAuth.getCurrentUser().getUid());
                    matchName = "match" + matchID;
                    query.child("matchId").removeEventListener(this);
                } else {
                    Log.i(TAG, "getMatchId else: " + snapshot.getValue() + firebaseAuth.getCurrentUser().getUid());
                    matchID = 0;
                    matchName = "match" + matchID;
                    query.child("matchId").setValue(matchID);
                    query.child("matchId").removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }

    public void setMatch() {
        query.child("matchId").setValue(matchID + 1);
    }

    @Override
    public void onClick(View v) {
        if (v == btnCancel) {
            //delete waiting operation from database
            mDatabase.child("matchmaking").child("queue").child("player1").removeValue();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if ( lOnCreate != null)
            query.child("queue").child("player1").removeEventListener(lOnCreate);
        if ( lcheckLobby != null)
            query.child(matchName).child("go").removeEventListener(lcheckLobby);
        if ( lcheckMatch != null)
            query.child(matchName).child("go").removeEventListener(lcheckMatch);
        if ( lgetMatch != null)
            query.child("matchId").removeEventListener(lgetMatch);

    }

}
