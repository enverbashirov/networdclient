package com.netword.jrjkr.netword;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{

    private Button buttonLogin;
    private CheckBox boxSave;
    private CheckBox boxShowPass;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextView textViewRegistration;

    private FirebaseAuth firebaseAuth;

    private SharedPreferences sp;
    private SharedPreferences.Editor Ed;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        sp = getSharedPreferences("UserDate", 0);
        Ed = sp.edit();

        if (firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity( new Intent(getApplicationContext(), MenuActivity.class));
        }

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        boxSave = (CheckBox) findViewById(R.id.boxSave);
        boxShowPass = (CheckBox) findViewById(R.id.boxShowPass);
        textViewRegistration = (TextView) findViewById(R.id.textViewRegistration);

        if ( sp.contains("username") && sp.contains("password")) {
            editTextEmail.setText(sp.getString("username", ""));
            editTextPassword.setText(sp.getString("password", ""));
        }

        buttonLogin.setOnClickListener(this);
        textViewRegistration.setOnClickListener(this);

        boxShowPass.setOnCheckedChangeListener( this);
    }

    private void userLogin() {
        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        Toast.makeText(this, "Logging in...", Toast.LENGTH_SHORT).show();

        firebaseAuth.signInWithEmailAndPassword( email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if( task.isSuccessful()) {
                            if ( boxSave.isChecked()) {
                                Ed.putString("username",email );
                                Ed.putString("password",password);
                                Ed.commit();
                            } else
                                Ed.clear().apply();

                            finish();
                            startActivity( new Intent(getApplicationContext(), MenuActivity.class));
                        }
                        else
                            Toast.makeText(LoginActivity.this, "Failed log in. Check email and password!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if ( view == buttonLogin) {
            userLogin();
        }

        if ( view == textViewRegistration) {
            finish();
            startActivity( new Intent(this, MainActivity.class));
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if ( isChecked) {
            editTextPassword.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            editTextPassword.setInputType(129);
        }
    }
}
