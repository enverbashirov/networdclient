package com.netword.jrjkr.netword;

import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by JrJKR on 10/12/2016.
 *
 * Last Edited on 10/12/2016
 * Cheers
 */

interface UserImplementer {

    //GETTERS
    String getFirstname();
    String getSurname();
    String getUsername();
    String getCompressedAvatar();
    Bitmap getBitmapAvatar();
    String getId();

    //SETTERS
    void setFirstname( String firstname);
    void setSurname( String surname);
    void setUsername( String username);
    void setAvatar(Bitmap bitmapAvatar);
    void setAvatar(String compressedAvatar);
    void setId( String id);

    void setUser( User user);
    void setUser( String firstname, String surname, String username, Bitmap bitmapAvatar, String id);

    //METHODS
    void downloadInfo( String uid);
    void uploadInfo();
    void uploadInfo(String uid);

//    void uploadToCatalog(User user);

    Bitmap getCircularBitmap(Bitmap bitmap);
    void getAvatarView(final ImageView view, final Integer x, final Integer y);
    void getWelcomeIntro(final TextView v, String uid, final ImageView iv, final Integer x, final Integer y);
}
