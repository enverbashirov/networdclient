package com.netword.jrjkr.netword;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.IBinder;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MenuActivity\t||";
    private ImageButton btnVolControl;
    private Button btnPlay;

    private SearchView searchView;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    private Toolbar mToolbar;
    private boolean mIsBound = false;

    private Intent music = new Intent( this, MusicService.class);
    private MusicService mServ;
    private ServiceConnection Scon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        if ( firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity( new Intent( this, ProfileActivity.class));
        }
        User mUser = new User();
        mUser.downloadInfo(firebaseAuth.getCurrentUser().getUid());

        mUser.getWelcomeIntro((TextView) findViewById(R.id.user_profile_info), firebaseAuth.getCurrentUser().getUid(), (ImageView) findViewById(R.id.user_profile_photo), 204, 204);
        ((TextView) findViewById(R.id.welcome_text)).setText("You are the best!");

        mUser.getAvatarView((ImageView) findViewById(R.id.user_profile_photo), 204, 204);

        btnVolControl = (ImageButton) findViewById(R.id.btnVolControl);
        btnVolControl.setOnClickListener( this);

        btnPlay = (Button) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener( this);

        //START NAVIGATION PANEL
        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();

        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        //NAVIGATION PANEL =====

        Scon = new ServiceConnection(){
            public void onServiceConnected(ComponentName name, IBinder
                    binder) {
                mServ = ((MusicService.ServiceBinder)binder).getService();
            }

            public void onServiceDisconnected(ComponentName name) {
                mServ = null;
            }
        };
        doBindService();
        startService(music);
    }

    @Override
    public void onClick(View v) {
        if (v == btnVolControl) {
            if ( mServ.checkPlaying()) {
                btnVolControl.setImageResource(R.drawable.ic_action_volume_mute);
                mServ.pauseMusic();
            } else {
                btnVolControl.setImageResource(R.drawable.ic_action_volume_up);
                mServ.resumeMusic();
            }
        } else if (v == btnPlay) {
            startActivity( new Intent(this, WaitingActivity.class));
        }
    }

    private void addDrawerItems() {
        final String[] friendsArray = { "Ali", "Tuna", "Cihan", "Osman", "Enver" };
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friendsArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if ( position == 4)
                    Toast.makeText(MenuActivity.this, "Such engineer, much intelligence!", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MenuActivity.this, "He is somewhat decent guy.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("Example Buddies");
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);

        mDrawerLayout.addDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.user_profile:
                startActivity( new Intent(this, ProfileActivity.class));
            case R.id.search:
                return false;
                // nothing to be done here
            default:
                break;
        }
        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mToolbar.inflateMenu(R.menu.menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String word) {
                //search
                Log.d(TAG, "querySubmit: " + word);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "query: " + newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        mServ.onDestroy();
        doUnbindService();
        super.onDestroy();
    }

    void doBindService(){
        bindService(new Intent(this, MusicService.class),
                Scon, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }
    void doUnbindService(){
        if(mIsBound)
        {
            unbindService(Scon);
            mIsBound = false;
        }
    }
}
